//
// Good old JS module implemented within self-invoking function, created for demonstration purposes only
//

const dialogModule = (function() {
  // We can implement sth like private functions and variables inside scope
  // of self-invoking function, and return an object with public interface

  // Functions show and ask are private.
  function show(message) {
    alert(message);
  }

  function aks(...args) {
    return prompt(...args);
  }

  function askIndexOrName() {
    return aks("Please, enter index or exact name");
  }

  function askMovieDetails(initialData) {
    return aks("Coma separated: name, director, year", initialData);
  }

  function showNotFound(movie) {
    show(`Movie ${movie} was not found`);
  }

  function showCommandCancelled(movie) {
    show(`Command cancelled`);
  }

  return {
    askIndexOrName,
    askMovieDetails,
    showNotFound,
    showCommandCancelled
  };
})();

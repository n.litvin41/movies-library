//
// Helpers
//

export function capitalie(string) {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

export function toSentence(string) {
  return string
    .split(/(?=[A-Z])/)
    .map(capitalie)
    .join(" ");
}

export function prettyPrintObject(data) {
  return Object.keys(data).reduce((acc, key) => {
    const value = data[key];
    const formatted = value instanceof Date ? value.toLocaleString() : value;

    return `${acc}
      ${toSentence(key)}: ${formatted}`;
  }, "");
}

//
// Functions to work with movies data
//

let movies = [];

export function addMovie(movie) {
  movies.push({
    ...movie,
    createdAt: new Date()
  });
}

export function printMovies() {
  return movies.reduce((acc, movie) => {
    return `${acc}
      ${prettyPrintObject(movie)}
    `;
  }, "");
}

export function findMovie(indexOrName) {
  // if prompted index
  if (!isNaN(indexOrName)) {
    if (indexOrName >= movies.length || indexOrName < 0) {
      return { ok: false };
    }

    return {
      index: +indexOrName,
      movie: movies[indexOrName],
      ok: true
    };
  }

  // if prompted name
  for (let i = 0; i < movies.length; i++) {
    if (movies[i].name === indexOrName) {
      return {
        index: i,
        movie: movies[i],
        ok: true
      };
    }
  }

  return { ok: false };
}

export function updateMovie(data, indexToUpdate) {
  movies = movies.map((movie, index) =>
    index === indexToUpdate
      ? { ...movie, ...data, updatedAt: new Date() }
      : movie
  );
}

export function deleteMovie(indexToDelete) {
  movies = movies.filter((movie, index) => index !== indexToDelete);
}

export function printDirectors() {
  const directors = movies.map(({ director }) => director);
  const uniqueDirectors = new Set(directors);

  let message = "";

  for (let director of uniqueDirectors) {
    message += `
      ${director}`;
  }

  return message;
}

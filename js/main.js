import {
  addMovie,
  deleteMovie,
  printMovies,
  updateMovie,
  printDirectors,
  findMovie
} from "./movies.js";

const COMMANDS = "create, print, update, delete, directors, exit";

const {
  askIndexOrName,
  askMovieDetails,
  showNotFound,
  showCommandCancelled
} = dialogModule;

//
// Helpers
//

function mapParamstoMovie(paramsString) {
  const [name, director, year] = paramsString.split(",");

  return {
    name,
    director,
    year
  };
}

//
// Command handlers
//

function handleAddMovie() {
  const params = askMovieDetails();

  if (!params) {
    showCommandCancelled();
    return;
  }

  const movie = mapParamstoMovie(params);
  addMovie(movie);
}

function handlePrintMovies() {
  const message = printMovies();
  alert(message);
}

function handleUpdateMovie() {
  const indexOrName = askIndexOrName();
  const { movie, index, ok } = findMovie(indexOrName);

  // guard expression instead if/else
  if (!ok) {
    showNotFound(indexOrName);
    return;
  }

  const initialData = `${movie.name}, ${movie.director}, ${movie.year}`;
  const params = askMovieDetails(initialData);

  if (!params) {
    showCommandCancelled();
    return;
  }

  const updatedMovie = mapParamstoMovie(params);
  updateMovie(updatedMovie, index);
}

function handleDeleteMovie() {
  const indexOrName = askIndexOrName();
  const { index, ok } = findMovie(indexOrName);

  if (!ok) {
    showNotFound(indexOrName);
    return;
  }

  deleteMovie(index);
}

function handlePrintDirectors() {
  const message = printDirectors();
  alert(message);
}

//
// Command loop, the heart of this application
//

function init() {
  let keepGoing = true;

  while (keepGoing) {
    const command = prompt(`Allowed commands: ${COMMANDS}`);

    switch (command) {
      case "create":
        handleAddMovie();
        break;

      case "print":
        handlePrintMovies();
        break;

      case "update":
        handleUpdateMovie();
        break;

      case "delete":
        handleDeleteMovie();
        break;

      case "directors":
        handlePrintDirectors();
        break;

      case "exit":
        keepGoing = false;
        break;

      default:
        alert(`Command ${command} is not allowed, please try: ${COMMANDS}!`);
    }
  }
}

const initButton = document.querySelector("#init-btn");

document.addEventListener("DOMContentLoaded", () => {
  initButton.addEventListener("click", init);
});

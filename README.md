# Movies Library

Simple CRUD application with alert and prompt based UI. Before running it make sure you have npm installed.
It is also accessible by a public link on [gitlab pages](https://n.litvin41.gitlab.io/movies-library/).

## Install dependencies and run

```sh
  # install serve package to run a local server
  npm install

  # start application
  npm start
```
